package com.example.lynn.dragview;

import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import static com.example.lynn.dragview.MainActivity.*;

/**
 * Created by lynn on 6/20/2016.
 */
public class MyListener implements View.OnClickListener,View.OnTouchListener {
    private RelativeLayout.LayoutParams layoutParams;
    private int offsetX;
    private int offsetY;

    @Override
    public void onClick(View v) {
        if (v instanceof Button) {
            Button source = (Button)v;
        }


    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        layoutParams = (RelativeLayout.LayoutParams)v.getLayoutParams();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            offsetX = (int)(event.getRawX() - layoutParams.leftMargin);
            offsetY = (int)(event.getRawY() - layoutParams.topMargin);
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            layoutParams.leftMargin = (int)(event.getRawX() - offsetX);
            layoutParams.topMargin = (int)(event.getRawY() - offsetY);

            v.setLayoutParams(layoutParams);
        }

        return false;
    }
}
