package com.example.lynn.dragview;

import android.content.Context;
import android.widget.Button;
import android.widget.RelativeLayout;

import static com.example.lynn.dragview.MainActivity.*;

/**
 * Created by lynn on 6/20/2016.
 */
public class MyView extends RelativeLayout {

    public MyView(Context context) {
        super(context);

        setBackground(getResources().getDrawable(R.drawable.background));

        button = new Button(context);

        button.setBackground(getResources().getDrawable(R.drawable.circle));

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(200,200);

        button.setLayoutParams(layoutParams);

        button.setOnTouchListener(listener);

        addView(button);
    }

}
